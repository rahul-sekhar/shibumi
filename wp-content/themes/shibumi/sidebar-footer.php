<?php
/**
 * The footer area
 *
 * @package WordPress
 * @subpackage Shibumi
 * @since Shibumi 1.0
 */
?>

  <!-- Footer block -->
  <footer id="page-footer">
    <div class="wrapper">
      <section id="contact">
        <h3>Contact</h3>

        <?php if ( get_option( 'shibumi-campus-address' )) { ?>
        <div class="column">
          <h4>Campus address:</h4>
          <address><?php echo nl2br( esc_html( get_option( 'shibumi-campus-address' ))) ?></address>
        </div>
        <?php } ?>

        <?php if ( get_option( 'shibumi-mailing-address' )) { ?>
        <div class="column">
          <h4>Mailing address:</h4>
          <address><?php echo nl2br( esc_html( get_option( 'shibumi-mailing-address' ))) ?></address>
        </div>
        <?php } ?>

        <div class="column">
          <?php if ( get_option( 'shibumi-email' )) { ?>
          <h4>Email</h4>
          <p><?php echo c2c_obfuscate_email( esc_html( get_option( 'shibumi-email' ))) ?></p>
          <?php } ?>

          <?php if ( get_option( 'shibumi-phone' )) { ?>
          <h4>Phone</h4>
          <p><?php echo nl2br( esc_html( get_option( 'shibumi-phone' ))) ?></p>
          <?php } ?>
        </div>
      </section>

      <?php dynamic_sidebar( 'footer' ); ?>
    </div>
  </footer>