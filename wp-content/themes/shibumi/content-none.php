<?php
/**
 * The template for displaying a "No posts found" message.
 *
 * @package WordPress
 * @subpackage Shibumi
 * @since Shibumi 1.0
 */
?>

	<article id="post-0" class="post no-results not-found">
		<header class="entry-header">
			<h1 class="entry-title">Nothing Found</h1>
		</header>

		<div class="entry-content">
			<p>No results were found.</p>
		</div>
	</article>
