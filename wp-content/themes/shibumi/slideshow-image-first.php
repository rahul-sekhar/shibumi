<?php
/**
 * The template used for displaying a slideshow image for the front page
 *
 * @package WordPress
 * @subpackage Shibumi
 * @since Shibumi 1.0
 */
?>
  <div class="slide first">
    <?php if ( has_post_thumbnail() ) the_post_thumbnail( 'slideshow' ); ?>
    <div class="caption">
      <p><?php the_title(); ?></p>
    </div>
  </div>
