/* Shibumi school website - theme JS file */

$(document).ready(function() {
  
  // Open external links in a new window
  $('a.ext').attr('target', '_blank');

  // Open download section links in a new window
  $('.widget_wpfb_filelistwidget a').attr('target', '_blank');

  // Prepare slideshow
  var $slideshow = $('#slideshow');
  $slideshow.find('.slide:not(.first)').each(function() {
    var $slide = $(this);
    var image_url = $slide.data('image');
    $slide.prepend('<img src="' + image_url + '" alt="" width="876" height="385" />');
  });

  $slideshow.find('.caption p').each(function() {
    $(this).prepend('<a href="#" class="prev">&laquo;</a>')
      .append('<a href="#" class="next">&raquo;</a>');
  });

  $slideshow.slides({
    play: 5000,
    pause: 4000,
    generatePagination: false,
    effect: 'fade',
    crossfade: true
  });
  
  // Handle hideable content
  $('#content article h4:first').addClass('first');

  $('#content article h4').each(function() {
    var $heading = $(this);
    var $contentToHide = $heading.nextUntil('h4');
    var $contentDiv = $('<div></div>').addClass('hideable');
    
    $contentDiv.append($contentToHide)
      .insertAfter($heading)
      .hide();

    $heading.css('cursor', 'pointer').on('click', function() {
        $contentDiv.animate({ opacity: 'toggle', height: 'toggle' });
      });
  });

  // Open blog link in an external window
  $('#site-navigation').find('a[href*="blogspot"]')
    .attr('target', '_blank');
});