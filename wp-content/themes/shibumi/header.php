<?php
/**
 * Theme header
 *
 * @package WordPress
 * @subpackage Shibumi
 * @since Shibumi 1.0
 */
?><!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<?php
	// Get background image
	$page_title = trim(wp_title( '', false ));

	if ($page_title != '') { // Skip the home page
		$background_post = get_page_by_title( $page_title, 'OBJECT', 't_background' );
		if (!$background_post) {
			$background_post = get_page_by_title( 'Default', 'OBJECT', 't_background' );
		}
		$background_thumbnail_id = get_post_thumbnail_id( $background_post->ID );
		$background_exists = (bool)($background_thumbnail_id);
	}
	
	if ($background_exists) {
		$background_image_data = wp_get_attachment_image_src( $background_thumbnail_id, 'full' );
		$background_src = $background_image_data[0];
		$background_width = $background_image_data[1];
	}
?>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><?php bloginfo( 'name' ); ?> <?php wp_title( '|' ); ?></title>
		<meta name="description" content="<?php bloginfo( 'description' ) ?>">
		<meta name="viewport" content="width=device-width" />

		<link rel="shortcut icon" href="/favicon.ico?v=1" />
		
		<?php wp_head(); ?>

		<?php if ($background_exists) { ?>
		<!-- Background image dynamic styling -->
		<style type="text/css" media="screen">
			#background-image { min-width: <?php echo $background_width ?>px; }
			@media screen and (max-width: <?php echo $background_width ?>px) {
				#background-image {
					left: 50%;
					margin-left: -<?php echo ($background_width / 2) ?>px;
				}
			}
		</style>
		<?php } ?>

		<!-- Modernizr script -->
		<script src="<?php echo get_template_directory_uri(); ?>/js/lib/modernizr-2.6.2.min.js"></script>
		
		<!-- Other scripts -->
		<script src="<?php echo get_template_directory_uri(); ?>/js/lib/jquery-1.8.3.min.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/lib/slides.min.jquery.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/main.js?v=3"></script>

		<!-- Typekit scripts -->
		<script src="//use.typekit.net/wrg3wsa.js"></script>
		<script>try{Typekit.load();}catch(e){}</script>
	</head>

	<body <?php body_class(); ?>>
		
		<?php if ($background_exists) { ?>
		<img id="background-image" src="<?php echo $background_src ?>" alt="" />
		<?php } ?>
		<div id="top-bar"></div>

		<div id="page-wrapper">
			<div id="page">

				<div id="header-nav">
					<!-- Header block -->
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" id="masthead-link">
						<header id="masthead" role="banner">
								<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
								<h1 class="site-title"><?php bloginfo( 'name' ); ?></h1>
						</header>
					</a>

					<!-- Navigation block -->
					<nav id="site-navigation" class="main-navigation" role="navigation">
						<a class="assistive-text" href="#content" title="Skip to content">Skip to content</a>
						<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
					</nav>
				</div>