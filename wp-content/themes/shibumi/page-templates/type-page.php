<?php
/*
Template Name: Page for a particular type
*/

get_header(); ?>

      <div id="content" role="main">
        <hgroup id="page-heading">
          <h2><?php single_post_title() ?></h2>
        </hgroup>

        <?php $post_type = get_post_meta($posts[0]->ID, 'type', true); ?>
        <?php $args = array(
        	'post_type' => $post_type,
        	'posts_per_page' => -1,
          'orderby' => 'menu_order',
          'order' => 'ASC'
        ); ?>
        <?php query_posts($args); ?>
        
        <?php if ( have_posts() ) : ?>

          <?php /* Start the Loop */ ?>
          <?php while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'content-page', get_post_type() ); ?>
          <?php endwhile; ?>

        <?php endif;  // end have_posts() check ?>
      </div><!-- #content -->

<?php get_footer(); ?>
