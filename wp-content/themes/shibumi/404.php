<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Shibumi
 * @since Shibumi 1.0
 */

get_header(); ?>
			
			<div id="content" role="main">
				<hgroup id="page-heading">
          <h2>Page not found</h2>
        </hgroup>

				<article id="post-0" class="post error404">
					<div class="entry-content">
						<p>The page you were looking for cannot be found.</p>
					</div>
				</article>

			</div><!-- #content -->

<?php get_footer(); ?>