<?php
/**
 * The template for displaying the footer.
 *
 *
 * @package WordPress
 * @subpackage Shibumi
 * @since Shibumi 1.0
 */
?>
      </div><!-- #page -->

      <div id="footer-section">
        <?php get_sidebar('footer'); ?>

        <section id="credit">
          <div class="arrow"></div>
          <a href="http://kairi.in" class="ext">
            <p>
              Design: <span class="person">Shalini Sekhar</span><br />
              Development: <span class="person">Rahul Sekhar</span>
            </p>
          </a>
        </section>
      </div>

    </div><!-- #page-wrapper -->

    <?php wp_footer(); ?>
  </body>
</html>