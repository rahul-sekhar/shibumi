<?php
/**
 * The template used for displaying a slideshow image for the front page
 *
 * @package WordPress
 * @subpackage Shibumi
 * @since Shibumi 1.0
 */
?>
  <?php $image_src = "" ?>
  <?php if ( has_post_thumbnail() ) { ?>
    <?php // Get image URL ?>
    <?php $thumb_id = get_post_thumbnail_id(); ?>
    <?php $image_data = wp_get_attachment_image_src( $thumb_id, 'slideshow' ); ?>
    <?php $image_src = $image_data[0]; ?>
  <?php } ?>
  <div class="slide" data-image="<?php echo $image_src ?>">
    <div class="caption">
      <p><?php the_title(); ?></p>
    </div>
  </div>
