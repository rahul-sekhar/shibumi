<?php
/**
 * Shibumi functions and definitions.
 *
 * Sets up the theme and provides some helper functions, which are used
 * in the theme as custom template tags. Others are attached to action and
 * filter hooks in WordPress to change core functionality.
 *
 * @package WordPress
 * @subpackage Shibumi
 * @since Shibumi 1.0
 */

/**
 * Sets up the content width value based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 625;

/**
 * Sets up theme defaults and registers the various WordPress features that
 * Shibumi supports.
 *
 * @uses load_theme_textdomain() For translation/localization support.
 * @uses add_editor_style() To add a Visual Editor stylesheet.
 * @uses add_theme_support() To add support for post thumbnails, automatic feed links,
 * 	custom background, and post formats.
 * @uses register_nav_menu() To add support for navigation menus.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 *
 * @since Shibumi 1.0
 */
function shibumi_setup() {
	// This theme uses wp_nav_menu() in one location.
	register_nav_menu( 'primary', 'Primary Menu' );

  // Enable post thumbnails (used for post type - t_person)
  add_theme_support( 'post-thumbnails', array( 't_person', 't_slideshow', 't_background', 't_blog' ) );

  // Add an image size for slideshow images
  add_image_size( 'slideshow', 876, 385, true );

  // Set up post types for the various sections

  // 'Slideshow photos'
  register_post_type( 't_slideshow',
    array(
      'labels' => array(
        'name' => 'Slideshow Photos',
        'singular_name' => 'Slideshow Photo'
      ),
      'public' => true,
      'supports' => array( 'title', 'thumbnail' )
    )
  );

  // 'Background images'
  register_post_type( 't_background',
    array(
      'labels' => array(
        'name' => 'Background Images',
        'singular_name' => 'Background Image'
      ),
      'public' => true,
      'supports' => array( 'title', 'thumbnail' )
    )
  );

  // 'Understanding Shibumi'
  register_post_type( 't_understanding',
    array(
      'label' => 'Understanding Shibumi Content',
      'public' => true
    )
  );

  // 'Educational Process'
  register_post_type( 't_process',
    array(
      'label' => 'Educational Process Content',
      'public' => true
    )
  );

  // 'Practical Matters'
  register_post_type( 't_practical',
    array(
      'label' => 'Practial Matters Content',
      'public' => true
    )
  );

  // 'Engaging with Shibumi'
  register_post_type( 't_engaging',
    array(
      'label' => 'Engaging with Shibumi Content',
      'public' => true
    )
  );

  // 'People'
  register_post_type( 't_person',
    array(
      'labels' => array(
        'name' => 'People',
        'singular_name' => 'Person'
      ),
      'public' => true,
      'supports' => array( 'title', 'editor', 'thumbnail' )
    )
  );

  // 'Introduction'
  register_post_type( 't_introduction',
    array(
      'label' => 'Introduction Content',
      'public' => true,
      'supports' => array( 'editor' )
    )
  );
}
add_action( 'after_setup_theme', 'shibumi_setup' );

/**
 * Enqueues scripts and styles for front-end.
 *
 * @since Shibumi 1.0
 */
function shibumi_scripts_styles() {
	global $wp_styles;

	/*
	 * Loads our main stylesheet.
	 */
	wp_enqueue_style( 'shibumi-style', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'shibumi_scripts_styles' );

/**
 * Inserts the styleselect dropdown for TinyMCE
 */
function shibumi_mce_buttons_2( $buttons ) {
  array_unshift( $buttons, 'styleselect' );
  return $buttons;
}
add_filter('mce_buttons_2', 'shibumi_mce_buttons_2');



/**
 * Insert the custom styles
 */
function shibumi_mce_before_init_insert_formats( $init_array ) {  
  // Define the style_formats array
  $style_formats = array(  
    array(  
      'title' => 'highlighted',  
      'inline' => 'span',  
      'classes' => 'highlight',
      'wrapper' => true
    )
  );  
  $init_array['style_formats'] = json_encode( $style_formats );  
  
  return $init_array;  
  
} 
// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'shibumi_mce_before_init_insert_formats' );  

// Add a stylesheet for the tinyMCE editor
add_editor_style('editor-style.css');


/**
 * Registers the footer widget area
 */
function shibumi_widgets_init() {
  register_sidebar( array(
    'name' => 'Footer',
    'id' => 'footer',
    'description' => 'This should only contain a Downloads widget, please do not add anything else',
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget' => '</section>',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
  ) );

}
add_action( 'widgets_init', 'shibumi_widgets_init' );

/* Set up the settings page for contact info */
function shibumi_admin_menu() {
    add_options_page( 
      'Contact Information', 
      'Contact Information', 
      'manage_options', 
      'contact-information-page', 
      'shibumi_contact_information_page' 
    );
}
add_action( 'admin_menu', 'shibumi_admin_menu' );

function shibumi_admin_init() {
    register_setting( 'contact-information', 'shibumi-campus-address' );
    register_setting( 'contact-information', 'shibumi-mailing-address' );
    register_setting( 'contact-information', 'shibumi-email' );
    register_setting( 'contact-information', 'shibumi-phone' );
    
    add_settings_section( 'contact-section', 'Settings', 'shibumi_contact_section_callback', 'contact-information-page' );
    
    add_settings_field( 'shibumi-campus-address-field', 
      'Campus Address', 
      'shibumi_textarea_callback', 
      'contact-information-page', 
      'contact-section',
      array(
        'name' => 'shibumi-campus-address'
      )
    );
    add_settings_field( 'shibumi-mailing-address-email-field', 
      'Mailing Address', 
      'shibumi_textarea_callback', 
      'contact-information-page', 
      'contact-section',
      array(
        'name' => 'shibumi-mailing-address'
      )
    );
    add_settings_field( 'shibumi-email-field', 
      'Email', 
      'shibumi_email_callback', 
      'contact-information-page', 
      'contact-section'
    );
    add_settings_field( 'shibumi-phone-field', 
      'Phone', 
      'shibumi_textarea_callback', 
      'contact-information-page', 
      'contact-section',
      array(
        'name' => 'shibumi-phone'
      )
    );
}
add_action( 'admin_init', 'shibumi_admin_init' );

function shibumi_contact_section_callback() {
    echo 'The contact information here is displayed on the footer of each page';
}

function shibumi_email_callback() {
    $email = esc_attr( get_option( 'shibumi-email' ) );
    echo '<input type="text" name="shibumi-email" value="' . $email . '" size="40" />';
}

function shibumi_textarea_callback( $args ) {
    $name = $args['name'];
    $text = esc_textarea( get_option( $name ) );
    echo '<textarea name="' . $name . '" rows="5" cols="40">' . $text . '</textarea>';
}

function shibumi_contact_information_page() {
  echo '<div class="wrap">';
    echo '<h2>Contact Information</h2>';
    echo '<form action="options.php" method="POST">';
      settings_fields( 'contact-information' );
      do_settings_sections( 'contact-information-page' );
      submit_button();
    echo '</form>';
  echo "</div>";
}