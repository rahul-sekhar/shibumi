<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
      <div id="slideshow">
        <div class="slides_container">
          <?php $args = array(
            'post_type' => 't_slideshow',
            'posts_per_page' => -1,
            'orderby' => 'menu_order',
            'order' => 'ASC'
          ); ?>
          <?php query_posts($args); ?>
          
          <?php if ( have_posts() ) : ?>

            <?php /* Start the slideshow loop */ ?>
            <?php $post_marker = 'first' // Mark the first slideshow image ?>
            <?php while ( have_posts() ) : the_post(); ?>

              <?php get_template_part( 'slideshow-image', $post_marker); ?>
              <?php $post_marker = NULL // Do not mark subsequent images ?>

            <?php endwhile; ?>

          <?php endif;  // end have_posts() check ?>
        </div>
      </div>
      
			<div id="content" role="main">
        <hgroup id="page-heading">
          <h2>Introduction</h2>
        </hgroup>
		    
        <?php $args = array(
          'post_type' => 't_introduction',
          'posts_per_page' => 2,
          'orderby' => 'menu_order',
          'order' => 'ASC'
        ); ?>
        <?php query_posts($args); ?>
        
        <?php if ( have_posts() ) : ?>

          <?php /* Start the Loop */ ?>
          <?php $n = 0; ?>
          <?php while ( have_posts() ) : the_post(); ?>
            <?php $n++; ?>
            <div id="intro-column-<?php echo $n; ?>">
              <?php get_template_part( 'content-single-page'); ?>
            </div>
          <?php endwhile; ?>

        <?php endif;  // end have_posts() check ?>
			</div><!-- #content -->

<?php get_footer(); ?>