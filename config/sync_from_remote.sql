USE shibumi_wp;  
UPDATE wp_options SET option_value = replace(option_value, 'http://shibumi.dreamhosters.com', 'http://shibumi.dev') 
    WHERE option_name = 'home' OR option_name = 'siteurl';