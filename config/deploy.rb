set :application, "shibumi"

default_run_options[:pty] = true
set :repository,  "git@github.com:rahul-sekhar/shibumi.git"
set :scm, :git
set :ssh_options, { forward_agent: true }

set :user, "kairistudio"
set :use_sudo, false
set :deploy_to, "/home/#{user}/shibumi_wordpress"
set :host_name, "shibumi.dreamhosters.com"

server host_name, :app, :web, :db, :primary => true

after "deploy:restart", "deploy:cleanup"

# Sensitive data
namespace :sensitive_data do
  
  desc "Copies the wp-config file to the server"
  task :setup, :roles => [:app, :web] do
    upload("wp-config.php", "#{shared_path}/wp-config.php")
  end
  
  desc "Updates symlinks for the wp-config file"
  task :copy_for_release, :roles => [:app, :web] do
    run "cp #{shared_path}/wp-config.php #{release_path}/wp-config.php"
  end
end

after "deploy:create_symlink", "sensitive_data:copy_for_release"

# Shared assets
set :shared_asset_paths, ['wp-content/uploads']

namespace :shared_assets  do
  namespace :symlinks do
    desc "Setup application symlinks for shared assets"
    task :setup, :roles => [:app, :web] do
      shared_asset_paths.each { |link| run "mkdir -p #{shared_path}/#{link}" }
    end

    desc "Link assets for current deploy to the shared location"
    task :update, :roles => [:app, :web] do
      shared_asset_paths.each { |link| run "ln -nfs #{shared_path}/#{link} #{release_path}/#{link}" }
    end
  end
  
  desc "Import shared objects from the remote server to the local machine"
  task :import_from_remote, :roles => :app do
    shared_asset_paths.each do |link|
      system "rsync -rP #{user}@#{host_name}:#{shared_path}/#{link} #{link}/.."
    end
  end
  
  desc "Export shared objects from the local machine to the remote server"
  task :export_to_remote, :roles => :app do
    shared_asset_paths.each do |link|
      system "rsync -rP #{link} #{user}@#{host_name}:#{shared_path}/#{link}/.."
    end
  end
end

before "deploy:setup", "shared_assets:symlinks:setup"
before "deploy:create_symlink", "shared_assets:symlinks:update"